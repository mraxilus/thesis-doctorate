#!/usr/bin/env python3

"""TODO: Write documentation."""

import datetime as dt
import os
import sys

import pandas as pd

# Setup local paths and modules.
PATH_FILE = globals().get("__file__")
PATH_PREFIX = (
    sys.path[0] if not PATH_FILE else os.path.dirname(os.path.realpath(PATH_FILE))
)
PATH_PREFIX_CLEAN = f"{PATH_PREFIX}/../clean"
sys.path.append(f"{PATH_PREFIX}/../../../source/thesis")
import thesis.functions as ts_ft  # noqa: E402

# Configure inputs.
PATH_RAW = f"{PATH_PREFIX}/../raw/meter_readings_by_block.xlsx"
TIME_WINDOW_SIZE = "30min"

# Configure outputs.
PATH_UNIVARIATE = f"{PATH_PREFIX_CLEAN}/univariate.parquet"
PATH_MULTIVARIATE = f"{PATH_PREFIX_CLEAN}/multivariate.parquet"


def main() -> None:
    """Generate univariate and mulitvariate water meter readings dataset.

    The univariate dataset is the sum of the readings from all meters on campus.
    The multivarite dataset is the sum of the readings from all meters per building.
    """
    readings = pd.read_excel(PATH_RAW)

    # Tidy column names.
    readings = readings.rename(
        columns={
            "Date": "weekday",
            "Unnamed: 4": "month_and_day",
            "Unnamed: 5": "year",
        }
    )
    readings = ts_ft.format_columns_snake_case(readings, columns=readings.columns[:6])

    # Combine month/day and year columns, as month/day has assumed year.
    readings["date"] = readings.apply(
        lambda r: r["month_and_day"].replace(year=r["year"]), axis=1
    )

    # Reform readings into a more common format.
    columns_non_value = set(readings.columns[:6])
    columns_keep = {"date", "meter"}
    readings_melted = readings.drop(columns=columns_non_value - columns_keep).melt(
        id_vars=columns_keep, var_name="time"
    )

    # Account for day rollover for datetime conversion.
    #     I.e. 00:00 should be assigned to the next day, not the current one.
    readings_melted["timestamp"] = readings_melted["date"] + pd.to_timedelta(
        readings_melted["time"].astype(str)
    )
    readings_melted["timestamp"] = readings_melted["timestamp"].mask(
        readings_melted["timestamp"].dt.time == dt.time(0, 0),
        readings_melted["timestamp"] + pd.to_timedelta("1d"),
    )

    # Cleanup dataframe naming and ordering.
    readings_clean = (
        readings_melted.sort_values(["date", "time", "meter"])
        .drop(columns=["date", "time"])
        .set_index("timestamp")
    )

    # Unmelt readings into a form suitable form suitable for modelling.
    readings_by_block = readings_clean.pivot(columns="meter", values="value")
    readings_by_block = ts_ft.format_columns_snake_case(readings_by_block)

    # Group readings by total for building.
    readings_by_building = pd.DataFrame(
        index=readings_clean.sort_index().index.unique()
    )
    buildings = {m.split("_")[0] for m in readings_by_block.columns}
    for building in sorted(buildings):
        blocks = readings_by_block.columns[
            readings_by_block.columns.str.contains(building)
        ]
        readings_by_building[building] = readings_by_block[blocks].sum(axis="columns")
    readings_by_building.columns.name = "building"

    # Group readings by total for campus.
    readings_total = pd.DataFrame(
        readings_by_building.sum(axis="columns"), columns=["value"]
    )
    readings_total.columns.name = "campus"

    # Save cleaned readings.
    readings_by_building.to_parquet(PATH_MULTIVARIATE)
    readings_total.to_parquet(PATH_UNIVARIATE)


if __name__ == "__main__":
    main()
