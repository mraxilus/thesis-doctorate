#!/usr/bin/env python3

"""TODO: Write documentation."""
import os
import sys

import numpy as np
import pandas as pd
from sklearn.preprocessing import normalize  # type: ignore

# Setup local paths and modules.
PATH_FILE = globals().get("__file__")
PATH_PREFIX = (
    sys.path[0] if not PATH_FILE else os.path.dirname(os.path.realpath(PATH_FILE))
)
PATH_PREFIX_CLEAN = f"{PATH_PREFIX}/../clean"
sys.path.append(f"{PATH_PREFIX}/../../../source/thesis")

# Configure outputs.
PATH_UNIVARIATE = f"{PATH_PREFIX_CLEAN}/univariate.parquet"
PATH_MULTIVARIATE = f"{PATH_PREFIX_CLEAN}/multivariate.parquet"


def main() -> None:
    """Generate univariate and mulitvariate synthetic dataset.

    The multivarite dataset include synthetic signals as three seperate features.
    These features map to a synthetic daily, weekly, and monthly signal respectively.
    The univariate dataset contains a single feature,
        which is a combination of those in the univariate case.
    The methods for combination is a normalizatoin of: (daily + weekly) * monthly.
    """
    # Generate indiviudal synthetic signals.
    daily = make_square(
        steps=24,
        tiles=7 * 4 * 12,
        offset=0,
    )
    weekly = normalise_wave(
        make_saw(
            steps=24 * 7,
            tiles=4 * 12,
            offset=0,
        )
    )
    monthly = make_sine(
        steps=24 * 30 * 2,
        tiles=6,
        offset=0,
    )[: len(weekly)]

    # Determine signal dates.
    date_index = pd.date_range(
        start="2018-01-01", freq="1h", periods=len(daily), name="timestamp"
    )

    # Create univariate dataset.
    combined_univariate = make_frame((daily + weekly) * np.abs(monthly)) / 2
    combined_univariate.columns = ["value"]
    combined_univariate = combined_univariate.set_index(date_index).rename_axis(
        columns="synthetic"
    )

    # Create feature-normalised multivariate dataset.
    combined_multivariate = pd.DataFrame(
        {
            "daily": daily * 2 - 1,
            "weekly": weekly * 2 - 1,
            "monthly": monthly,
        }
    )
    combined_multivariate = combined_multivariate.set_index(date_index).rename_axis(
        columns="synthetic"
    )

    # Save datasets.
    combined_univariate.to_parquet(PATH_UNIVARIATE)  # type: ignore
    combined_multivariate.to_parquet(PATH_MULTIVARIATE)  # type: ignore


def get_random_normal(mean, deviation, minimum=np.nan, maximum=np.nan):
    """TODO: Document/annotate function."""
    result = np.random.normal(mean, deviation)
    result = min(result, maximum)
    result = max(result, minimum)
    return result


def normalise_wave(values):
    """Normalise values for optimal neural network inputs."""
    if values.ndim == 1:
        values = values.reshape((-1, 1))
        values = normalize(values, norm="max", axis=0)
        values = values.reshape(-1)
    else:
        values = normalize(values, norm="max", axis=0)
    return values


def make_wave(values, tiles=1, offset=None):
    """Generate repeating wave pattern from given values."""
    # Randomly generate phase offset if not given.
    if offset is None:
        length = len(values)
        offset = int(get_random_normal(length // 2, length // 2, 0, length - 1))

    # Repeat wave pattern and apply offset.
    values = np.tile(values, tiles)
    values = np.roll(values, offset, axis=0)

    return values


def make_sine(steps=10, tiles=1, offset=0, frequency=1):
    """Generate a repreating sine wave pattern."""
    times = np.linspace(0, tiles, int(steps * tiles))
    values = np.sin(2 * np.pi * frequency * times)
    return make_wave(values, 1, offset)


def make_square(steps=10, tiles=1, offset=None):
    """Generate a repreating square wave pattern."""
    values = np.concatenate((np.zeros(steps // 2), np.ones(steps // 2)))
    return make_wave(values, tiles, offset)


def make_saw(steps=10, tiles=1, offset=None):
    """Generate a repreating saw wave pattern."""
    values = np.arange(steps)
    return make_wave(values, tiles, offset)


def make_triangle(steps=10, tiles=1, offset=None):
    """Generate a repreating triangle wave pattern."""
    values = np.concatenate((np.arange((steps // 2) + 1), np.arange(steps // 2)[:0:-1]))
    return make_wave(values, tiles, offset)


def combine_waves(a, b, tiles=1, offset=None, normalise=True):
    """Combine two waves to be presented as input features."""
    # Normalise waves to same scale and range.
    if normalise:
        a, b = normalise_wave(a), normalise_wave(b)

    min_length = min(len(a), len(b))
    values = np.column_stack((a[:min_length], b[:min_length]))
    return make_wave(values, tiles=(tiles, 1), offset=offset)


def attach_waves(a, b, tiles=1, offset=None, normalise=True):
    """Attach one wave to the tail end of another."""
    # Normalise waves to same scale and range.
    if normalise:
        a, b = normalise_wave(a), normalise_wave(b)

    values = np.concatenate((a, b))
    return make_wave(values, tiles=tiles, offset=offset)


def make_frame(wave):
    """TODO: Document/annotate function."""
    return pd.DataFrame(wave, np.arange(len(wave)))


if __name__ == "__main__":
    main()
