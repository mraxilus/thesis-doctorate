#!/usr/bin/env python3

"""TODO: Write documentation."""

from __future__ import annotations

import os
import sys

import numpy as np
import pandas as pd

# Setup local paths and modules.
PATH_FILE = globals().get("__file__")
PATH_PREFIX = (
    sys.path[0] if not PATH_FILE else os.path.dirname(os.path.realpath(PATH_FILE))
)
PATH_PREFIX_CLEAN = f"{PATH_PREFIX}/../clean"
PATH_PREFIX_RAW = f"{PATH_PREFIX}/../raw/"
sys.path.append(f"{PATH_PREFIX}/../../../source/thesis/")
import thesis.functions as ts_ft  # type: ignore # noqa: E402

# Configure inputs.
PATHS = [
    f"{PATH_PREFIX_RAW}/{y}.csv" for y in range(2017, 2021)
]  # Use subset of years for memory/storage efficiency.
COLUMNS_FORMATTED = [
    "date",
    "time",
    "o3",
    "o3_status",
    "no",
    "no_status",
    "no2",
    "no2_status",
    "nox",
    "nox_status",
    "so2",
    "so2_status",
    "co",
    "co_status",
    "pm10",
    "pm10_status",
    "pm10_non_volitile",
    "pm10_non_volitile_status",
    "pm10_volitile",
    "pm10_volitile_status",
    "pm2_5",
    "pm2_5_status",
    "pm2_5_non_volitile",
    "pm2_5_non_volitile_status",
    "pm2_5_volitile",
    "pm2_5_volitile_status",
    "wind_direction",
    "wind_direction_status",
    "wind_speed",
    "wind_speed_status",
    "temperature",
    "temperature_status",
]
COLUMNS_NUMERIC = sorted(
    COLUMNS_FORMATTED[2::2]
)  # Every other column after date/time is numeric.
COLUMN_UNIVARIATE = "pm10"

# Configure outputs.
PATH_UNIVARIATE = f"{PATH_PREFIX_CLEAN}/univariate.parquet"
PATH_MULTIVARIATE = f"{PATH_PREFIX_CLEAN}/multivariate.parquet"


def main() -> None:
    """Generate univariate and multivariate air quality readings dataset.

    The univariate dataset includes only the high-level PM10 readings.
    The multivariate dataset includes all uncorrelated high-level readings.
    """
    # Import raw sensor readings.
    readings = pd.concat(
        [
            pd.read_csv(
                p,
                engine="python",  # Use Python engine for skipfooter option.
                skiprows=4,  # Avoid parsing initial metadata.
                skipfooter=1,  # Ignore file end delimeter.
            )
            for p in PATHS
        ]
    )

    # Tidy column names/types.
    readings.columns = COLUMNS_FORMATTED
    readings[COLUMNS_NUMERIC] = readings[COLUMNS_NUMERIC].apply(
        pd.to_numeric, errors="coerce"
    )

    # Simplify related fields.
    readings["timestamp"] = pd.to_datetime(readings["date"]) + pd.to_timedelta(
        readings["time"]
    )
    readings["wind_vector_x"], readings["wind_vector_y"] = ts_ft.project_angle_to_axes(
        np.deg2rad(readings["wind_direction"]), scale=readings["wind_speed"]
    )

    # Clean readings into a form suitable for modelling.
    columns_modelling = ["timestamp"] + sorted(
        list(
            set(COLUMNS_NUMERIC + ["wind_vector_x", "wind_vector_y"])
            - {
                "no",
                "no2",
                "pm10_volitile",
                "pm10_non_volitile",
                "pm2_5",
                "pm2_5_volitile",
                "pm2_5_non_volitile",
                "wind_direction",
                "wind_speed",
            }
        )
    )
    readings_clean = readings[columns_modelling]
    readings_clean = readings_clean.set_index("timestamp").sort_index()
    readings_clean = readings_clean.resample(
        "1h"
    ).max()  # For air quality, we care about peak reading in given period.
    readings_clean = readings_clean.fillna(
        method="ffill"
    ).fillna(  # Assume missing values are last seen value.
        method="bfill"
    )  # Except for beginning where we take value from future.
    readings_clean.columns.name = "sensor"

    # Create univariate series for modelling.
    readings_univariate = (
        readings_clean[[COLUMN_UNIVARIATE]]
        .rename(columns={COLUMN_UNIVARIATE: "value"})
        .rename_axis(columns=COLUMN_UNIVARIATE)
    )

    # Save cleaned readings.
    readings_clean.to_parquet(PATH_MULTIVARIATE)
    readings_univariate.to_parquet(PATH_UNIVARIATE)


if __name__ == "__main__":
    main()
