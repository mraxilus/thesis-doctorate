---
default_install_hook_types: [commit-msg, post-checkout, pre-commit, pre-push]
minimum_pre_commit_version: "2.18.0"

# TODO: Refactor notebooks/dashboard files.
#       Remove this exclusion after refactoring.
exclude: "^source/thesis/dashboard|notebook"

repos:
  # Perform general checks.
  - repo: https://github.com/pre-commit/pre-commit-hooks
    rev: v4.2.0
    hooks:
      - id: check-case-conflict
      - id: check-executables-have-shebangs
      - id: check-merge-conflict
      - id: check-shebang-scripts-are-executable
      - id: detect-private-key
      - id: end-of-file-fixer
      - id: mixed-line-ending
      - id: trailing-whitespace

  # Check commit messages.
  - repo: https://github.com/compilerla/conventional-pre-commit
    rev: v1.3.0
    hooks:
      - id: conventional-pre-commit
        stages: [commit-msg]

  # Check web related files.
  - repo: https://github.com/pre-commit/mirrors-prettier
    rev: v2.6.2
    hooks:
      - id: prettier
        args: ["--ignore-path", ".gitignore"]
        types_or:
          - html
          - markdown
          - yaml
          - toml
          - json

  # Check DVC files.
  - repo: https://github.com/iterative/dvc
    rev: 2.10.1
    hooks:
      - id: dvc-pre-commit
        language_version: python3
        stages:
          - commit
      - id: dvc-pre-push
        additional_dependencies: [".[all]"]
        language_version: python3
        stages:
          - push
      - id: dvc-post-checkout
        language_version: python3
        stages:
          - post-checkout
        always_run: true

  # Check Markdown files.
  - repo: https://github.com/executablebooks/mdformat
    rev: 0.7.14
    hooks:
      - id: mdformat

  # Check Python files.
  - repo: https://github.com/PyCQA/bandit # Check security vulnerabilites.
    rev: 1.7.4
    hooks:
      - id: bandit
  - repo: https://github.com/psf/black # Standardize code style.
    rev: 22.3.0
    hooks:
      - id: black
  - repo: https://github.com/PyCQA/flake8 # Indentify errors.
    rev: 4.0.1
    hooks:
      - id: flake8
        args: [--max-line-length=88]
  - repo: https://github.com/PyCQA/isort # Sort imports.
    rev: 5.10.1
    hooks:
      - id: isort
        args: [--profile, black]
  - repo: https://github.com/pre-commit/mirrors-mypy # Indentify type errors.
    rev: v0.942
    hooks:
      - id: mypy
        additional_dependencies: [types-pyyaml==6.0.5]
  - repo: https://github.com/PyCQA/pydocstyle # Check docstrings.
    rev: 6.1.1
    hooks:
      - id: pydocstyle
        additional_dependencies: [toml==0.10.2]
        args: [--add-select=D212]
  - repo: https://github.com/asottile/pyupgrade # Ensure modern syntax.
    rev: v2.32.0
    hooks:
      - id: pyupgrade
        args: [--py38-plus]

  # Check Python notebooks.
  - repo: https://github.com/nbQA-dev/nbQA
    rev: 1.3.1
    hooks:
      - id: nbqa-flake8
        args: [--max-line-length=88]
      - id: nbqa-isort
        args: [--profile, black]
      - id: nbqa-mypy
      - id: nbqa-pydocstyle
        args: [--add-select=D212]
      - id: nbqa-pyupgrade
        args: [--py38-plus]
