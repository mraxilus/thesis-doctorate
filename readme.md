# [Doctoral Thesis](https://people.uwe.ac.uk/Person/SinclairSmith)

🎓 _Undertaking a doctoral thesis from the University of the West of Englands's PhD programme._

## 🔧 Usage

This project is managed with `pyenv` for environments, `poetry` for dependencies, and `dvc` for reproducability.
If you're having issues, both `.python-version` and `pyproject.toml` should be enough for manual replication.
To install the dependencies run:

```sh
# Install pyenv.
curl https://pyenv.run | bash

# Install poetry.
curl -sSL https://install.python-poetry.org | python3 -

# Restart shell to finish installations.
exec $SHELL
```

If you have both dependencies installed, simply run:

```sh
# Setup environment via pyenv and Poetry.
pyenv install $(cat .python-version)
poetry install --no-dev
poetry shell
```

You should then have an active shell environment with everything you need to replicate our results.
To actually go about doing so, run:

```python
dvc repro --pull
```

If you have any issues please contact [sinclair.smith@uwe.ac.uk](mailto:sinclair.smith@uwe.ac.uk).

## 📝 Reporting

Almost all of the outputs of this research were written in LaTeX.
If you want access to the original files:

```sh
git submodule update --init --recursive
```

Please note, as of writing the submodules are password protected.
Due to the specific tooling used for the writing process,
this will remain the case until the research concludes.
Upon sucessful completion, these files will be made asscesible and read-only.

## 🧪 Testing

GitLab runs automatic tests whenever changes are pushed to the repository.
It checks to see if the remote dataset cache is available,
and tries to replicate the results using the process described above.

If you want to reproduce the testing process locally, run:

```sh
gitlab-runner exec docker replicate
```

This requires that you have both [Docker](https://docs.docker.com/engine/install/) and [GitLab Runner](https://docs.gitlab.com/runner/install/) installed.
Installation instructions are outside the scope of this project.
