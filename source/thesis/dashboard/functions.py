"""Provide helper functions for dashboard visualisations."""

import pandas as pd
import plotly.graph_objects as pl_go
import thesis.types as ts_tp


def to_lines(
    parameterized: pd.DataFrame, parameterization: ts_tp.Parameterization
) -> pd.DataFrame:
    """Extract time-series data for a given parameterization."""
    return parameterized.xs(
        (parameterization.method, parameterization.size, parameterization.function),
        level=("method", "size", "function"),
    )


def plot_lines(
    lines: pd.Series,
    parameterization: ts_tp.Parameterization,
    *,
    name: str = "Selected",
) -> pl_go.Figure:  # type: ignore
    """Produce line visualisation of time-series data."""
    # Generate trace for each time-series feature.
    figure = pl_go.Figure()
    for feature in lines.columns:
        figure.add_trace(
            pl_go.Scatter(
                x=lines.index,
                y=lines[feature],
                hovertemplate=f"%{{y:.2f}}",
                name=feature,
            )
        )

    # Prepare text for figure labels.
    function = (
        f"{parameterization.function.title()}"  # type: ignore
        if parameterization.function
        else ""
    )
    method = (
        f"{parameterization.method.title()}" if parameterization.method else ""  # type: ignore
    )
    size = f"{parameterization.size}" if parameterization.size else ""  # type: ignore

    # Customize figure with desired layout.
    return figure.update_layout(
        xaxis_title="Date",
        yaxis_title=function,
        legend_title_text="Feature",
        hovermode="x unified",
        title=f"{function} of {name.title()} Data using {size}h {method} Aggregation",
    )
