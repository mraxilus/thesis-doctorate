#!/usr/bin/env python3


"""Make predictions across timesteps in parameterized data using RNN modelling."""

from __future__ import annotations

import logging as lg
import typing as tp

import hug
import numpy as np
import pandas as pd
import thesis.functions as ts_ft
import thesis.types as ts_tp

logger = lg.getLogger(__name__)


@hug.cli()
def main(path_source: str, path_destination: str) -> pd.DataFrame:
    """Produce RNN predicitons from parameterized data.

    Where a seperate model is trained for each combination of parameters.
    """
    path_project = ts_ft.get_project_directory()
    return ts_ft.generate_data_with_parameterizations(
        function=predict_rnn_for_parameterizations,
        paths_source=[f"{path_project}/{path_source}"],
        path_destination=f"{path_project}/{path_destination}",
    )


def predict_rnn_for_parameterizations(
    data: pd.DataFrame, /, *, parameterizations: tp.Set[ts_tp.Parameterization]
) -> pd.DataFrame:
    """Fit and make predictions from RNN models for each parameterization.

    Each parameterization runs in parallel depnedant on the number of cores.
    """
    return ts_ft.map_across_parameterizations(
        predict_rnn_for_parameterization,
        data=data,
        parameterizations=parameterizations,
        type_index=ts_tp.IndexPrediction,
    )


def predict_rnn_for_parameterization(
    data: pd.DataFrame, /, parameterization: ts_tp.Parameterization
) -> pd.DataFrame:
    """Fit and make predictions from RNN model for specific parameterization.

    A train-test spit is made, where an initial base model is trained.
    For each timestep, on the test data, the model:
        - is retrained to include all preceeding timesteps.
        - has to predict ahead for a certain amount of timesteps in the future.
    """
    logger.info(f"{parameterization}.")

    # TODO: Prepare and split data into train/test sets.

    # TODO: Train initial model on training set.

    # TODO: Make in-sample predicitons, using initial model and train set.

    # TODO: Make out-of-sample predictions, using retrained models and test set.

    # TODO: Combine resulting predictions into appropriate format.

    return pd.DataFrame()


def predict_in_sample(
    data_chunk: pd.DataFrame,
    data_rnn: pd.DataFrame,
    size_train: int,
    model_base: str,
    parameterization: ts_tp.Parameterization,
    timesteps_ahead: int,
) -> pd.DataFrame:
    """Make in-sample predictions for parameterized data."""
    columns_index = ts_ft.get_index_names(ts_tp.IndexPrediction)
    result = pd.DataFrame(columns=columns_index)

    for i in range(size_train):
        # TODO: Make look-ahead predictions and add to results.
        pass

    return result


def predict_out_of_sample(
    data_chunk: pd.DataFrame,
    data_rnn: pd.DataFrame,
    size_test: int,
    size_train: int,
    model_base: str,
    parameterization: ts_tp.Parameterization,
    timesteps_ahead: int,
) -> pd.DataFrame:
    """Make out-of-sample predictions for parameterized data."""
    columns_index = ts_ft.get_index_names(ts_tp.IndexPrediction)
    result = pd.DataFrame(columns=columns_index)

    for i in range(size_test):
        logger.debug(f"{parameterization}\t({i+1:>5}/{size_test:<5}).")

        # TODO: Retrain base model including all preceeding timesteps.

        # TODO: Make look-ahead predictions and add to results.

    return result


def to_rnn_from_parameterized(inputs: pd.DataFrame) -> np.ndarray:
    """Convert parameterized dataset into format suitable for RNN inputs.

    Where inputs should only include a singular combination of parameters.
    """
    result = inputs.droplevel(["method", "size", "function"]).reset_index()

    # TODO: Convert date to RNN appropriate features.

    # TODO: Normalize inputs between -1 and 1.

    return result  # type: ignore


def to_parameterized_from_rnn(
    outputs: np.ndarray, /, *, parameterization: ts_tp.Parameterization
) -> pd.DataFrame:
    """Convert RNN ouputs into format matching parameterized dataset.

    Where outputs should only include a singular combination of parameters.
    """
    # TODO: Reverse RNN conversions and normalizations.

    return pd.DataFrame()


if __name__ == "__main__":
    main.interface.cli()
