#!/usr/bin/env python3

"""Make predictions across timesteps in parameterized data using Prophet modelling."""

from __future__ import annotations

import datetime as dt
import logging as lg
import os
import typing as tp
import warnings as wn

import hug
import pandas as pd
import prophet as pp
import thesis.functions as ts_ft
import thesis.types as ts_tp

logger = lg.getLogger(__name__)

# Attempt to clean up noisy Prophet/PyStan outputs.
lg.getLogger("prophet").setLevel(lg.ERROR)
lg.getLogger("pystan").setLevel(lg.ERROR)
wn.simplefilter(action="ignore", category=FutureWarning)


@hug.cli()
def main(path_source: str, path_destination: str) -> pd.DataFrame:
    """Produce Prophet predicitons from parameterized data.

    Where a seperate model is trained for each combination of parameters.
    """
    logger.info("Generate Prophet predictions from parameterized data.")
    path_project = ts_ft.get_project_directory()
    return ts_ft.generate_data_with_parameterizations(
        function=predict_prophet_for_parameterizations,
        paths_source=[f"{path_project}/{path_source}"],
        path_destination=f"{path_project}/{path_destination}",
    )


def predict_prophet_for_parameterizations(
    data: pd.DataFrame, /, *, parameterizations: tp.Set[ts_tp.Parameterization]
) -> pd.DataFrame:
    """Fit and make predictions from Prophet models for each parameterization.

    Each parameterization runs in parallel depnedant on the number of cores.
    """
    return ts_ft.map_across_parameterizations(
        predict_prophet_for_parameterization,
        data=data,
        parameterizations=parameterizations,
        type_index=ts_tp.IndexPrediction,
    )


def predict_prophet_for_parameterization(
    data: pd.DataFrame, /, parameterization: ts_tp.Parameterization
) -> pd.DataFrame:
    """Fit and make predictions from Prophet model for specific parameterization.

    A train-test spit is made, where an initial base model is trained.
    For each timestep, on the test data, the model:
        - is retrained to include all preceeding timesteps.
        - has to predict ahead for a certain amount of timesteps in the future.
    """
    # Prepare data for model training.
    data_chunk = data.query(
        f"""
        method == '{parameterization.method}' \
        & size == {parameterization.size} \
        & function == '{parameterization.function}' \
        """
    )
    data_prophet = to_prophet_from_parameterized(data_chunk)
    data_train, data_test = ts_ft.split_dataset(data_prophet)
    timesteps_ahead = int(ts_ft.load_parameters()["timesteps_ahead"])

    # Train base model.
    logger.info(f"{parameterization}\t(    0/{data_test.shape[0]:<5}).")
    model_base = pp.Prophet()
    with suppress_output():
        model_base.fit(data_train)

    # Make in-sample predicitons.
    forcast_in_sample = model_base.predict(
        model_base.make_future_dataframe(
            periods=timesteps_ahead,  # Include additional timesteps to avoid NaNs.
            freq=f"{parameterization.size}h",
            include_history=True,
        )
    )
    timestamp_in_sample_last = pd.Timestamp(data_train["ds"].max())  # type: ignore
    predictions_in_sample = (
        to_predictions_from_prophet_forcast(
            forcast_in_sample,
            parameterization=parameterization,
            timestamps=forcast_in_sample["ds"],
        )
        # Reformat in-sample forcast into prediction format.
        .set_index(ts_ft.get_index_names(ts_tp.IndexParameterization)).pipe(
            lambda d: ts_ft.to_prediction_actual_from_parameterized(d)
        )
        # Remove timesteps used to avoid NaNs.
        .pipe(lambda d: d[d["timestamp"] <= timestamp_in_sample_last])
        # Remove predictions for current timestep.
        .query("timestep > 0")
    )

    # Make out-of-sample predictions.
    predictions_out_of_sample = predict_out_of_sample(
        data_chunk=data_chunk,
        data_prophet=data_prophet,
        size_train=data_train.shape[0],
        size_test=data_test.shape[0],
        model_base=model_base,
        parameterization=parameterization,
        timesteps_ahead=timesteps_ahead,
    )

    # Combine all resulting predictions.
    return pd.concat(  # type: ignore
        [predictions_in_sample, predictions_out_of_sample],
        ignore_index=True,
    )


def predict_out_of_sample(
    data_chunk: pd.DataFrame,
    data_prophet: pd.DataFrame,
    size_train: int,
    size_test: int,
    model_base: pp.Prophet,
    parameterization: ts_tp.Parameterization,
    timesteps_ahead: int,
) -> pd.DataFrame:
    """Make out-of-sample predictions for parameterized data."""
    # Retrieve starting parameters from base model for retraining.
    parameters_model = get_prophet_parameters(model_base)

    # Retrain and predict for every timestep in test set.
    results: list[pd.DataFrame] = []
    for i in range(size_test):
        logger.debug(f"{parameterization}\t({i+1:>5}/{size_test:<5}).")  # noqa: E501

        # Retrain model including all preceeding timesteps.
        model = pp.Prophet()
        with suppress_output():
            model.fit(
                data_prophet.iloc[: size_train + i + 1],
                init=parameters_model,  # Save training time using previous model.
            )
        parameters_model = get_prophet_parameters(model)

        # Make look-ahead predictions.
        forcast = model.predict(
            model.make_future_dataframe(
                periods=timesteps_ahead,
                freq=f"{parameterization.size}h",
                include_history=False,
            )
        )
        predictions = to_predictions_from_prophet_forcast(
            forcast,
            parameterization=parameterization,
            # Provide current test date.
            timestamps=(data_chunk.iloc[size_train + i].name[-1]),
        )
        results.append(predictions)

    return pd.concat(results, ignore_index=True)


def to_prophet_from_parameterized(data: pd.DataFrame) -> pd.DataFrame:
    """Convert parameterized dataset into format suitable for Prophet models.

    Where data should only include a singular combination of parameters.
    """
    return (
        data.droplevel(["method", "size", "function"])
        .reset_index()
        .iloc[:, :2]  # TODO: Add multivariate support for Prophet stage.
        .set_axis(["ds", "y"], axis="columns")  # Rename columns to those expected.
        .astype({"y": float})  # Convert to basic float, otherwise Prophet complains.
        .ffill()  # Impute NaNs as prophet doesn't deal with them well.
        .backfill()  # Impute remaining NaNs at start of data.
    )


def get_prophet_parameters(model: pp.Prophet) -> dict:
    """Retrieve parameters from a trained Prophet model."""
    result = {}
    for p in ["k", "m", "sigma_obs"]:
        result[p] = model.params[p][0][0]
    for p in ["delta", "beta"]:
        result[p] = model.params[p][0]
    return result


def to_predictions_from_prophet_forcast(
    forcast: pd.DataFrame,
    /,
    *,
    parameterization: ts_tp.Parameterization,
    timestamps: tp.Union[dt.datetime, pd.Series],
    names_column: list[str] = ["value"],
    name_columns: str = "features",
) -> pd.DataFrame:
    """Convert Prophet forcast into a standardized model prediction format."""
    result = (
        forcast[["yhat"]]  # TODO: Handle multivariate case.
        .set_axis(names_column, axis="columns")  # type: ignore
        .rename_axis(columns=name_columns)
        .assign(
            method=parameterization.method,
            size=parameterization.size,
            function=parameterization.function,
            timestamp=timestamps,
            timestep=range(1, forcast.shape[0] + 1),
        )
    )

    # Drop timesteps column if redundant, i.e. since timestamps are all provided.
    if type(timestamps) == pd.Series:
        result = result.drop(columns=["timestep"])  # type: ignore

    return result


class suppress_output:
    """See `https://github.com/facebook/prophet/issues/223#issuecomment-326455744`."""

    def __init__(self):
        """Open a pair of null files."""
        self.files_null = [os.open(os.devnull, os.O_RDWR) for _ in range(2)]

        # Save the actual output file descriptors.
        self.stdout = os.dup(1)
        self.stderr = os.dup(2)

    def __enter__(self):
        """Assign the null pointers to stdout and stderr."""
        os.dup2(self.files_null[0], 1)
        os.dup2(self.files_null[1], 2)

    def __exit__(self, *_):
        """Re-assign the real stdout/stderr back to (1) and (2)."""
        os.dup2(self.stdout, 1)
        os.dup2(self.stderr, 2)
        for file in self.files_null + [self.stdout, self.stderr]:
            os.close(file)


if __name__ == "__main__":
    main.interface.cli()
