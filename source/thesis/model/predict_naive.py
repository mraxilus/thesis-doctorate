#!/usr/bin/env python3

"""Generate naive predicitons for parameterized dataset."""

import logging as lg
import typing as tp

import hug  # type: ignore
import pandas as pd
import thesis.functions as ts_ft
import thesis.types as ts_tp

logger = lg.getLogger(__name__)


@hug.cli()
def main(path_source: str, path_destination: str) -> pd.DataFrame:
    """Produce naive predicitons from parameterized data.

    Where naive predictions means that the model repeats the last presented value.
    We consider this naive,
        in the sense that the model predicts that the future as equilavent to the past.
    """
    logger.info("Generate naive predictions from parameterized data.")
    path_project = ts_ft.get_project_directory()
    return ts_ft.generate_data_with_parameterizations(
        function=predict_for_parameterizations,
        paths_source=[f"{path_project}/{path_source}"],
        path_destination=f"{path_project}/{path_destination}",
    )


def predict_for_parameterizations(
    data: pd.DataFrame, /, *, parameterizations: tp.Set[ts_tp.Parameterization]
) -> pd.DataFrame:
    """Fit and make predictions naively for each parameterization.

    Each parameterization runs in parallel depnedant on the number of cores.
    """
    return ts_ft.map_across_parameterizations(
        predict_for_parameterization,
        data=data,
        parameterizations=parameterizations,
        type_index=ts_tp.IndexPrediction,
    )


def predict_for_parameterization(
    data: pd.DataFrame, /, parameterization: ts_tp.Parameterization
) -> pd.DataFrame:
    """Generate naive predictions for parameterized data."""
    logger.debug(f"{parameterization}.")
    return (
        data.query(
            f"""
            method == '{parameterization.method}' \
            & size == {parameterization.size}  \
            & function == '{parameterization.function}'
            """
        )
        .pipe(ts_ft.to_prediction_repeated_from_parameterized)
        .query("timestep != 0")  # Drop original timestep since not prediction.
        .assign(
            method=parameterization.method,
            size=parameterization.size,
            function=parameterization.function,
        )
    )


if __name__ == "__main__":
    main.interface.cli()
