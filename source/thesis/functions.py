"""Provide a collection of reusable functions for use in other notebooks/modules."""

from __future__ import annotations

import functools as ft
import itertools as it
import logging as lg
import math
import os
import pathlib as pl
import sys
import typing as tp

import numpy as np
import pandas as pd
import psutil as pu
import pyarrow as pa
import ray
import thesis.types as ts_tp
import yaml

# Allow for flexible logging.
logger = lg.getLogger(__name__)


def map_across_parameterizations(
    function: tp.Callable,
    *,
    data: tp.Union[pd.DataFrame, list[pd.DataFrame]],
    parameterizations: tp.Set[ts_tp.Parameterization],
    type_index: type[ts_tp.Index],
    name_columns: str = "features",
    number_cores: int = pu.cpu_count() - 2,
) -> pd.DataFrame:
    """Run function for each parameterization in parallel.

    To give the function more than the paramerterization as an argument,
        use a partial function.
    The function should return a DataFrame with the provided index columns included,
        alongside at least one value column.
    The index column(s) of the DataFrame returned by the function will be ignored.
    The resulting DataFrame will then have its index columns set to those provided.
    """

    @ray.remote
    def f(d, p):
        """Provide proxy for Ray to prevent memory duplication."""
        return function(d, p)

    # Ensure results adhere to index format.
    columns_index = get_index_names(type_index)
    results = [pd.DataFrame(columns=columns_index)]

    # Remove unneeded parameters and deduplicate.
    parameterizations_stripped = strip_parameterizations(
        parameterizations,
        parameters_unneeded=set(get_parameter_names()) - set(columns_index),
    )

    # Run provided function across each parameterization in parrallel.
    logger.info(f"Spawn `{number_cores}`-process pool for parallel execution.")
    ray.init(num_cpus=number_cores)
    data_id = ray.put(data)
    results.extend(ray.get([f.remote(data_id, p) for p in parameterizations_stripped]))
    logger.info("Shutdown process pool.")
    ray.shutdown()

    # Standardize format of resulting DataFrame.
    logger.info(
        f"Combine ouptuts from all `{len(parameterizations)}` provided parameterizations."  # noqa: E501
    )
    return (
        pd.concat(results, ignore_index=True, copy=False)
        .rename_axis(name_columns, axis="columns")
        .set_index(columns_index)
        .sort_index()
    )


def estimate_core_availability(usage_memory: int) -> int:
    """Determine appropriate number of processes to spawn based on resources."""
    # Reduce core count if expected RAM usage per core is too high.
    estimate_cores_data = int(
        pu.virtual_memory().total / (usage_memory * 3)
    )  # Use manually-adjusted factor as memory ballons sometimes.

    # Limit memory based estimate to within available core count.
    estimate_cores = max(1, min(pu.cpu_count(), estimate_cores_data) - 1)

    logger.debug(f"Estimate resource availability for task at {estimate_cores} cores.")
    return estimate_cores


def format_columns_snake_case(
    data: pd.DataFrame, *, columns: tp.Optional[set] = None
) -> pd.DataFrame:
    """Format given column names as snake_case equivalent."""
    columns_ = columns if columns is not None else data.columns
    return data.rename(columns={c: c.lower().replace(" ", "_") for c in columns_})


def generate_data_with_parameterizations(
    *,
    function: tp.Callable[..., pd.DataFrame],
    paths_source: list[str],
    path_destination: str,
) -> pd.DataFrame:
    """Generate dataset from parameterized data using custom function.

    Allows the use of saving computation,
        such that parameters combinations are not recalculated.
    This allows additional parameter combinations to be efficiently added later.

    Data files should be in either CSV or Parquet format.
    They will be loaded and passed to the supplied function as positional arguments.
    The missing parameterizations will be supplied to the function as a  argument,
      namely, parameterizations.
    """
    # Load necessary parameters.
    parameterizations = get_parameterizations(load_parameters())
    use_cache = bool(load_parameters()["use_cache"])

    # Ignore cache requirement if cache file missing.
    path_cache = f"{path_destination}~"
    if use_cache and not os.path.exists(path_cache):
        if os.path.exists(path_destination):
            logger.warn(
                f"Use `{os.path.basename(path_destination)}` as cache since `{os.path.basename(path_cache)}` missing."  # noqa: E501
            )
            path_cache = path_destination
        else:
            logger.warn(
                f"Skip cache usage as neither `{os.path.relpath(path_cache)}` nor `{os.path.relpath(path_destination)}` exist."  # noqa: E501
            )
            use_cache = False

    # Load source data.
    data = []
    for path in paths_source:
        logger.info(f"Load source `{os.path.relpath(path)}` into memory.")
        data.append(pd.read_parquet(path))

    # Remove parameter combinations which have already been generated.
    if use_cache:
        logger.info(f"Load cache `{os.path.relpath(path_cache)}` into memory.")
        result_cached = pd.read_parquet(path_cache)
        parameterizations = generate_parameterizations_missing(
            result_cached, parameterizations=parameterizations
        )
        logger.info(
            f"Identify missing parameterization count as `{len(parameterizations)}`."
        )

        # Stop early if no missing parameterizations.
        if len(parameterizations) == 0:
            logger.debug("Skip computations as cache contains all parameterizations.")
            to_parquet_safe(result_cached, path_destination)
            return result_cached

    # Calculate/cache resulting data for all parameter combinations.
    logger.info(f"Compute results for {len(parameterizations)} parameterizations.")
    result = function(*data, parameterizations=parameterizations)
    if use_cache:
        result = pd.concat([result_cached, result]).sort_index()  # type: ignore
    to_parquet_safe(result, path_destination)  # type: ignore
    to_parquet_safe(result, path_cache)  # type: ignore

    return result  # type: ignore


def generate_parameterizations(
    *,
    methods: tp.Optional[set[str]],
    sizes: tp.Optional[set[int]],
    functions: tp.Optional[set[str]],
    metrics: tp.Optional[set[str]],
    moments: tp.Optional[set[str]],
) -> tp.Set[ts_tp.Parameterization]:
    """Generate every unique combination of aggregation parameterizations."""

    def convert_types(t: tp.Type, /, parameters: tp.Optional[set]) -> set:
        """Replace string values with associated parameter type."""
        return {t(p) for p in parameters} if parameters else {None}

    # Generate combinations using appropriate type conversions.
    combinations_parameters = it.product(
        convert_types(ts_tp.Method, methods),
        convert_types(ts_tp.Size, sizes),
        convert_types(ts_tp.Function, functions),
        convert_types(ts_tp.Metric, metrics),
        convert_types(ts_tp.Moment, moments),
    )

    # Produce final parameterization set.
    return {ts_tp.Parameterization(*ps) for ps in combinations_parameters}


def generate_parameterizations_missing(
    data: pd.DataFrame,
    *,
    parameterizations: tp.Set[ts_tp.Parameterization],
) -> tp.Set[ts_tp.Parameterization]:
    """Produce parameter combinations which don't exist in the source data."""
    # Determine which parameters are already present in the data.
    parameters_present = {
        n: get_unique_parameter_values(data, name=n) for n in get_parameter_names()
    }
    parameters_unneeded = {p for p, v in parameters_present.items() if v is None}

    # Regenerate existing parameterizations.
    parameterizations_existing = generate_parameterizations(
        **{f"{p}s": parameters_present[p] for p in get_parameter_names()}
    )

    # Ignore missing parameters for comparison.
    parameterizations_stripped = strip_parameterizations(
        parameterizations,
        parameters_unneeded=parameters_unneeded,
    )

    # Determine which parameterizations do not yet exist.
    return parameterizations_stripped - parameterizations_existing


def get_index_names(index: type[ts_tp.Index], /) -> list[str]:
    """Get index names as list for naming DataFrame MultiIndex columns."""
    return get_tuple_names(index)


def get_parameter_names() -> list[str]:
    """Get names of parameters within parameterization."""
    return get_tuple_names(ts_tp.Parameterization)


def get_script_directory() -> str:
    """Get directory of script/notebook."""
    file = globals().get("__file__")
    return sys.path[0] if not file else os.path.dirname(os.path.realpath(file))


def get_parameterizations(parameters: dict) -> set[ts_tp.Parameterization]:
    """Retrieve aggregation parameterizations from configuration file."""
    parameterizations = generate_parameterizations(
        **parameters["aggregations"],
        metrics=parameters["metrics"],
        moments=parameters["moments"],
    )
    return parameterizations


def get_project_directory() -> str:
    """Get top-level directory of project."""
    path_file = pl.Path(__file__)
    path_module = next(p for p in path_file.parents if p.name == "thesis")
    return f"{path_module}/../.."


def get_tuple_names(tuple_: type[tp.NamedTuple], /) -> list[str]:
    """Get names of tuple's arguments."""
    return list(vars(tuple_)["__annotations__"].keys())


def get_unique_parameter_values(
    data: pd.DataFrame, /, *, name: str
) -> tp.Optional[set]:
    """Get a specific parameter's unique values within dataset."""
    result = None

    # Attempt to retrieve unique values if level exists.
    try:
        result = set(data.index.get_level_values(name).unique())
    except KeyError:
        logger.debug(f"Skip identifying values for `{name}` as not in index.")

    return result


@ft.lru_cache
def load_parameters(path: str = f"{get_project_directory()}/params.yaml") -> dict:
    """Load parameters from DVC parameter file."""
    with open(path) as f:
        return yaml.safe_load(f)


def map_cycle_to_radians(
    data: tp.Union[pd.DataFrame, pd.Series], /
) -> tp.Union[pd.DataFrame, pd.Series]:
    """Map a numeric cyclical feature's values into radians.

    I.e. the resulting values will be between 0 and TAU.
    """
    data_normalised = (data - np.min(data)) / (np.max(data) - np.min(data) + 1)
    return math.tau * data_normalised


def project_angle_to_axes(
    angle: tp.Union[pd.DataFrame, pd.Series, np.ndarray],
    /,
    *,
    scale: tp.Union[float, np.ndarray, pd.Series] = 1.0,
) -> tuple[
    tp.Union[np.ndarray, pd.DataFrame, pd.Series],
    tp.Union[np.ndarray, pd.DataFrame, pd.Series],
]:
    """Project angle (in radians) onto scaled 2-dimensional axes."""
    return np.cos(angle) * scale, np.sin(angle) * scale


def project_cyclic_features_onto_axes(
    data: tp.Union[pd.DataFrame, pd.Series],
    /,
    *,
    columns: tp.Optional[list[str]] = None,
) -> pd.DataFrame:
    """Project cyclical features onto 2-dimensional axes.

    The purporse of this function is to maintain continuity in a cyclical feature.
    An example of this is hours in the day.
    I.e. hour 0 is as far away from 1 as it is from 23,
        so the feature should reflect this,
        rather than having large jumps arbitrarily.
    """
    # Perfrom projection.
    if isinstance(columns, list) and isinstance(data, pd.DataFrame):
        data_: tp.Union[pd.DataFrame, pd.Series] = data[columns]
    else:
        data_ = data
    axes: tuple[
        tp.Union[pd.DataFrame, pd.Series], tp.Union[pd.DataFrame, pd.Series]
    ] = project_angle_to_axes(
        map_cycle_to_radians(data_)
    )  # type: ignore

    # Ensure columns have unique names.
    for name, values in zip(("x", "y"), axes):
        if isinstance(data_, pd.DataFrame) and isinstance(values, pd.DataFrame):
            values.columns = [f"{c}_{name}" for c in data_.columns]
        elif isinstance(data_, pd.Series) and isinstance(values, pd.Series):
            values.name = f"{values.name}_{name}"

    # Merge axes into single dataframe.
    result = pd.concat(axes, axis="columns")
    return result[sorted(result.columns)]


def split_dataset(data: pd.DataFrame) -> tuple[pd.DataFrame, pd.DataFrame]:
    """Perform train-test split on dataset with proportions in DVC parameters."""
    proportion_split = float(load_parameters()["proportion_split"])
    point_split = int(data.shape[0] * proportion_split)
    return data[:point_split], data[point_split:]


def strip_parameters(
    parameterization: ts_tp.Parameterization,
    /,
    *,
    parameters_unneeded: set[str],
) -> ts_tp.Parameterization:
    """Replace parameters with none if stripping desired."""
    result = {p: getattr(parameterization, p) for p in get_parameter_names()}
    for p in parameters_unneeded:
        result[p] = None
    return ts_tp.Parameterization(**result)


def strip_parameterizations(
    parameterizations: set[ts_tp.Parameterization], /, *, parameters_unneeded: set[str]
) -> set[ts_tp.Parameterization]:
    """Remove selected parameters from parameter set with deduplication."""
    # Save performance if all parameters needed.
    if len(parameters_unneeded) == 0:
        return parameterizations

    # Ensure parameters unneeded are valid.
    parameters_valid = set(get_parameter_names())
    if not parameters_unneeded.issubset(parameters_valid):
        logger.warning(
            f"Ignore attempt to strip invalid parameter(s) for {parameters_unneeded}"
        )
        return parameterizations

    # Perform parameter stripping and deduplication.
    logger.debug(
        f"Strip {', '.join(f'`{p}`' for p in parameters_unneeded)} in parameterizations as unneeded."  # noqa: E501
    )
    return {
        strip_parameters(p, parameters_unneeded=parameters_unneeded)
        for p in parameterizations
    }


def to_parquet_safe(data: pd.DataFrame, path: str) -> None:
    """Write DataFrame to parquet file, even with custom objects.

    This is mainly so there's no need to manually convert parameters to strings.
    This result in errors with parameters such as method and function.
    """
    logger.info(f"Write `{os.path.relpath(path)}` to disk.")

    # Remember and reset multiindex columns for mass object conversion.
    names_index = data.index.names
    data_prepared = data.reset_index().convert_dtypes()

    # Convert all custom objects to string, and then catogory, dtype.
    data_converted = data_prepared
    for t in ["string", "category"]:
        converter = {
            c: t
            for c in data_prepared.select_dtypes(include=["object", "string"]).columns
        }
        data_converted = data_converted.astype(converter)

    # Restore index format and save.
    #   Need to use PyArrow intstead of `to_parquet` to avoid ValueError.
    #   This error happens whenever the column names aren't strings.
    data_table = pa.Table.from_pandas(
        data_converted.set_index(names_index).sort_index()
    )
    pa.parquet.write_table(data_table, path)


def to_prediction_actual_from_parameterized(data: pd.DataFrame) -> pd.DataFrame:
    """Split every sample into a set amount of look ahead timesteps.

    Where every look ahead timestep is what the actual timestep value should be.
    I.e. as observed in the data itself, thus creating a ground truth.
    """
    # Load necessary parameters.
    timesteps_ahead = int(load_parameters()["timesteps_ahead"])

    # Compute result.
    result = pd.DataFrame(columns=["timestamp", "timestep"])
    for i in range(timesteps_ahead + 1):
        data_shifted = data.shift(-i).assign(timestep=i).reset_index()
        result = pd.concat([result, data_shifted])
    return result


def to_prediction_repeated_from_parameterized(data: pd.DataFrame) -> pd.DataFrame:
    """Split every sample into a set amount of look ahead timesteps.

    Where every look ahead timestep is a repeat of the first value.
    The `data` must have an index in the form of `IndexParameterized`.
    """
    # Load necessary parameters.
    timesteps_ahead = int(load_parameters()["timesteps_ahead"])

    # Compute result.
    return (
        data.reset_index()
        .merge(pd.DataFrame(range(timesteps_ahead + 1)), how="cross")  # type: ignore
        .rename(columns={0: "timestep"})
    )
