#!/usr/bin/env python3

"""Generate resulting error between the ground truth and model predictions."""

from __future__ import annotations

import logging as lg
import typing as tp

import hug
import numpy as np
import pandas as pd
import thesis.functions as ts_ft
import thesis.types as ts_tp

logger = lg.getLogger(__name__)


@hug.cli()
def main(
    path_ground_truth: str, path_predictions: str, path_destination: str
) -> pd.DataFrame:
    """Generate resulting error across every parameterization in parallel."""
    path_project = ts_ft.get_project_directory()
    return ts_ft.generate_data_with_parameterizations(
        function=calculate_error_for_parameterizations,
        paths_source=[
            f"{path_project}/{path_ground_truth}",
            f"{path_project}/{path_predictions}",
        ],
        path_destination=f"{path_project}/{path_destination}",
    )


def calculate_error_for_parameterizations(
    ground_truth: pd.DataFrame,
    predictions: pd.DataFrame,
    /,
    *,
    parameterizations: tp.Set[ts_tp.Parameterization],
) -> pd.DataFrame:
    """Calculate the error between corresponding dataframe values.

    The caller must pass the specific error function used.
    """
    usage_memory = (
        ground_truth.memory_usage(deep=True).sum()
        + predictions.memory_usage(deep=True).sum()
    )
    return ts_ft.map_across_parameterizations(
        calculate_error_for_parameterization,
        data=[ground_truth, predictions],
        parameterizations=parameterizations,
        type_index=ts_tp.IndexError,
        number_cores=ts_ft.estimate_core_availability(usage_memory),
    )


def calculate_error_for_parameterization(
    data: list[pd.DataFrame], /, parameterization: ts_tp.Parameterization
) -> pd.DataFrame:
    """Calculate the error between corresponding dataframe values.

    For a specific parameterization, using a specific error function.
    """
    logger.debug(f"{parameterization}.")
    query = f"""
        method == '{parameterization.method}' \
        & size == {parameterization.size} \
        & function == '{parameterization.function}' \
        & timestep != 0
    """
    return (
        get_error_function(parameterization)(
            data[0].query(query),
            data[1].query(query),
        )
        .reset_index()  # Maintain indices upon aggregation.
        .assign(metric=parameterization.metric)
    )


def get_error_function(
    parameterization: ts_tp.Parameterization,
) -> tp.Callable[[pd.DataFrame, pd.DataFrame], pd.DataFrame]:
    """Retrieve the appropriate error calucalation function."""
    # Return default error function if no metric provided.
    if parameterization.metric is None:
        logger.warn(f"Metric was not specified for: {parameterization}")
        return calculate_root_relative_squared_error

    # Select appropriate error function based on metric.
    return {
        ts_tp.Metric.mae: calculate_absolute_error,
        ts_tp.Metric.rmse: calculate_root_squared_error,
        ts_tp.Metric.rrse: calculate_root_relative_squared_error,
        ts_tp.Metric.smape: calculate_relative_error,
    }[parameterization.metric]


def calculate_root_relative_squared_error(
    a: pd.DataFrame, b: pd.DataFrame
) -> pd.DataFrame:
    """Calculate the root relative squared error between each corresponding dataframe value.

    Where a is considered the target, and b the prediction.
    """
    return np.sqrt((a - b) ** 2 / (a - np.mean(a)) ** 2)


def calculate_absolute_error(a: pd.DataFrame, b: pd.DataFrame) -> pd.DataFrame:
    """Calculate the absolue error between each corresponding dataframe values."""
    return np.abs(a - b)  # type: ignore


def calculate_root_squared_error(a: pd.DataFrame, b: pd.DataFrame) -> pd.DataFrame:
    """Calculate the root squared error between each corresponding dataframe value."""
    return np.sqrt((a - b) ** 2)  # type: ignore


def calculate_relative_error(a: pd.DataFrame, b: pd.DataFrame) -> pd.DataFrame:
    """Calculate the symmetric absolute percentage error.

    Between each corresponding dataframe value.
    """
    return np.abs(a - b) / (np.abs(a) + np.abs(b))  # type: ignore


if __name__ == "__main__":
    main.interface.cli()
