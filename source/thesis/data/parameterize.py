#!/usr/bin/env python3

"""Generate a copy of a dataset in a format suitable for processing further."""

import logging as lg
import typing as tp

import hug  # type: ignore
import pandas as pd
import thesis.functions as ts_ft
import thesis.types as ts_tp

logger = lg.getLogger(__name__)


@hug.cli()
def main(path_source: str, path_destination: str) -> pd.DataFrame:
    """Apply all possible aggregation parameter combinations on source data.

    Saving the result.
    """
    logger.info("Parameterize source dataset with all provided aggregation parameters.")
    path_project = ts_ft.get_project_directory()
    return ts_ft.generate_data_with_parameterizations(
        function=parameterize_with_parameterizations,
        paths_source=[f"{path_project}/{path_source}"],
        path_destination=f"{path_project}/{path_destination}",
    )


def parameterize_with_parameterizations(
    data: pd.DataFrame, /, *, parameterizations: tp.Set[ts_tp.Parameterization]
) -> pd.DataFrame:
    """Perform every parameterized combination of aggregational choices on the data."""
    return ts_ft.map_across_parameterizations(
        parameterize_with_parameterization,
        data=data,
        parameterizations=parameterizations,
        type_index=ts_tp.IndexParameterization,
    )


def parameterize_with_parameterization(
    data: pd.DataFrame, /, parameterization: ts_tp.Parameterization
) -> pd.DataFrame:
    """Parameterize source data with a single combination of parameters."""
    logger.debug(f"{parameterization}.")
    return (
        data.pipe(lambda d: aggregate_with_parameterization(d, parameterization))
        .assign(
            method=parameterization.method,
            size=parameterization.size,
            function=parameterization.function,
        )
        .reset_index()  # Index is ignored; so needed to keep timestamp.
    )


def aggregate_with_parameterization(
    data: pd.DataFrame, /, parameterization: ts_tp.Parameterization
) -> pd.DataFrame:
    """Determine which aggregation approach to use and perform it."""
    result = pd.DataFrame()

    # Perform appropriate aggregation.
    function = str(parameterization.function)  # Avoid pandas error with explicit cast.
    if parameterization.method == ts_tp.Method.overlap:
        result = (
            data.resample("1h")  # Ensure standardized base timestep.
            .aggregate(function)
            .rolling(f"{parameterization.size}h", min_periods=1)  # Roll without NaNs.
            .aggregate(function)
        )
    elif parameterization.method == ts_tp.Method.isolate:
        result = data.resample(f"{parameterization.size}h", label="right").aggregate(
            function
        )
    else:
        logger.error(
            f"Ignore aggregation as unexpected method: `{parameterization.method}`"
        )

    return result


if __name__ == "__main__":
    main.interface.cli()
