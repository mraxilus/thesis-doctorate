#!/usr/bin/env python3

"""Compare two parquet files for equivalency."""

import hug as hg  # type: ignore
import pyarrow.parquet as pq  # type: ignore


@hg.cli()
def compare_parquet_files(
    filepaths: hg.types.multiple,
    extension: hg.types.text = "~",
) -> str:
    """Determine if two parquet files are equivalent.

    If the second file has the same filepath as the first,
      differing only with an extension (e.g. `a/b.c` and `a/b.c~`),
      you only have to provide the first filepath.

    If the extension parameter is not specified, it is assumed to be `~`.
    """
    if len(filepaths) == 0:
        return "No path provided, refer to documentation."

    # Autodetect second file if not provided.
    if len(filepaths) == 1:
        filepaths.append(f"{filepaths[0]}{extension}")

    # Compare files and return result.
    a = pq.read_table(filepaths[0])
    b = pq.read_table(filepaths[1])
    return "Files are equivalent." if a.equals(b) else "Files differ."


if __name__ == "__main__":
    compare_parquet_files.interface.cli()
