#!/usr/bin/env python3

"""Generate ground truth predictions from a parameterized dataset."""

import logging as lg
import typing as tp

import hug
import pandas as pd
import thesis.functions as ts_ft
import thesis.types as ts_tp

logger = lg.getLogger(__name__)


@hug.cli()
def main(path_source: str, path_destination: str) -> pd.DataFrame:
    """Produce ground truth predicitons from parameterized data."""
    logger.info("""Generate ground truth predictions from parameterized data.""")
    path_project = ts_ft.get_project_directory()
    return ts_ft.generate_data_with_parameterizations(
        function=establish_ground_truth_for_parameterizations,
        paths_source=[f"{path_project}/{path_source}"],
        path_destination=f"{path_project}/{path_destination}",
    )


def establish_ground_truth_for_parameterizations(
    data: pd.DataFrame, /, *, parameterizations: tp.Set[ts_tp.Parameterization]
) -> pd.DataFrame:
    """Generate ground truth predictions for parameterized data."""
    return ts_ft.map_across_parameterizations(
        establish_ground_truth_for_parameterization,
        data=data,
        parameterizations=parameterizations,
        type_index=ts_tp.IndexPrediction,
    )


def establish_ground_truth_for_parameterization(
    data: pd.DataFrame, /, parameterization: ts_tp.Parameterization
) -> pd.DataFrame:
    """Generate ground truth for a single parameterization."""
    logger.debug(f"{parameterization}.")
    return data.query(
        f"""
        method == '{parameterization.method}' \
        & size == {parameterization.size} \
        & function == '{parameterization.function}' \
        """
    ).pipe(ts_ft.to_prediction_actual_from_parameterized)


if __name__ == "__main__":
    main.interface.cli()
