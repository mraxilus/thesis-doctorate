#!/usr/bin/env python3

"""Convert prediction errors into an error surface.

Comparing aggregation size against timesteps ahead.
"""

import logging as lg
import typing as tp

import hug
import pandas as pd
import thesis.functions as ts_ft
import thesis.types as ts_tp

logger = lg.getLogger(__name__)


@hug.cli()
def main(path_source: str, path_destination: str) -> pd.DataFrame:
    """Convert errors into a format suitable for plotting an error surface."""
    path_project = ts_ft.get_project_directory()
    return ts_ft.generate_data_with_parameterizations(
        function=tabulate_errors_for_parameterizations,
        paths_source=[f"{path_project}/{path_source}"],
        path_destination=f"{path_project}/{path_destination}",
    )


def tabulate_errors_for_parameterizations(
    errors: pd.DataFrame, *, parameterizations: tp.Set[ts_tp.Parameterization]
) -> pd.DataFrame:
    """Create tabulated error table for each parameterization in parallel."""
    return ts_ft.map_across_parameterizations(
        tabulate_errors_for_parameterization,
        data=errors,
        parameterizations=ts_ft.strip_parameterizations(
            parameterizations,
            parameters_unneeded={"size"},  # Moving size from index to columns.
        ),
        type_index=ts_tp.IndexErrorSurface,
        name_columns="timesteps_ahead",
    )


def tabulate_errors_for_parameterization(
    errors: pd.DataFrame, /, parameterization: ts_tp.Parameterization
) -> pd.DataFrame:
    """Create table consisting of mean errors for each size/timestep combination."""
    logger.debug(f"{parameterization}.")

    # Filter out data specific to assigned parameterization.
    query = f"""
        method == '{parameterization.method}' \
        & function == '{parameterization.function}' \
        & metric == '{parameterization.metric}' \
    """
    errors_chunk = errors.query(query).reset_index()

    # Create error tabulation for each feature.
    result = []
    for feature in errors.columns:
        result.append(
            # Generate error surface.
            pd.crosstab(
                errors_chunk["size"],
                errors_chunk["timestep"],
                values=errors_chunk[feature],
                aggfunc=str(parameterization.moment),
            )
            # Add parameterization information.
            .assign(
                method=parameterization.method,
                function=parameterization.function,
                metric=parameterization.metric,
                moment=parameterization.moment,
                feature=feature,
            ).reset_index()
        )

    # Make into single Dataframe.
    return pd.concat(result, ignore_index=True)  # type: ignore


if __name__ == "__main__":
    main.interface.cli()
