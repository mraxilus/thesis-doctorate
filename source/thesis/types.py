"""Provids a standardized collection of types for use within the module."""

from __future__ import annotations

import datetime as dt
import enum as en
import typing as tp

# Define basic types.
Feature = str  # The name of the measurement.
Size = int  # The size (in hours) with which to aggregate the data.
Timestamp = dt.datetime  # The time associated with a particular measurment.
Timestep = int  # The relative position of each measurement from some initial timestamp.


# Define enumerations.
class Enum(en.Enum):
    """Customized enum with modified functions for convenience."""

    @staticmethod
    def _generate_next_value_(
        name: str, start: int, count: int, last_values: list
    ) -> str:
        """Automatically name values based off of their names using auto()."""
        return name.lower()

    def __str__(self: Enum) -> str:
        """Return the standardized name for use as dataframe column names."""
        return self.value


class Method(Enum):
    """The windowing approach to use when performing the aggregation process."""

    isolate = en.auto()  # independent or stepping.
    overlap = en.auto()  # overlapping or sliding.


class Function(Enum):
    """The function used for the data aggregation process."""

    max = en.auto()
    mean = en.auto()
    median = en.auto()
    min = en.auto()
    sum = en.auto()


class Metric(Enum):
    """The accuracy measures to use for prediction evaluations."""

    mae = en.auto()  # mean absolute error.
    rmse = en.auto()  # root mean squared error.
    rrse = en.auto()  # root relative squared error.
    smape = en.auto()  # symmetric mean absolute percentage error.


class Moment(Enum):
    """The accuarcy distribution's moment to use for prediction evaluations."""

    mean = en.auto()
    std = en.auto()
    skew = en.auto()


# Define classes.
class Parameterization(tp.NamedTuple):
    """The parameters for aggregating the data."""

    method: tp.Optional[Method]
    size: tp.Optional[Size]
    function: tp.Optional[Function]
    metric: tp.Optional[Metric]
    moment: tp.Optional[Moment]

    def __str__(self):
        """Allow for printing readable, aligned parameterization information."""
        results = []

        # Adjust format/spacing for every parameter.
        parameters = [
            ("method", self.method, 7),
            ("size", self.size, 3),
            ("function", self.function, 4),
            ("metric", self.metric, 4),
            ("moment", self.moment, 4),
        ]
        for name, value, spacing in parameters:
            results.append(f"{name} = {value:>{spacing}}" if value else "")

        return ", ".join(r for r in results if r != "")


class Index(tp.NamedTuple):
    """Base class for custom indicies used in module."""


class IndexParameterization(Index):
    """The index format for the parameterized data."""

    method: Method
    size: Size
    function: Function
    timestamp: Timestamp


class IndexPrediction(Index):
    """The index format for the prediction data."""

    method: Method
    size: Size
    function: Function
    timestamp: Timestamp
    timestep: Timestep


class IndexError(Index):
    """The index format for the prediction loss data."""

    method: Method
    size: Size
    function: Function
    metric: Metric
    timestamp: Timestamp
    timestep: Timestep


class IndexErrorSurface(Index):
    """The index format for the tabulated loss data."""

    method: Method
    function: Function
    metric: Metric
    moment: Moment
    feature: Feature
    size: Size
