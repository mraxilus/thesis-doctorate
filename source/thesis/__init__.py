"""TODO: Write module documentation."""

import logging as lg

from thesis import data, functions, model, types  # noqa: F401

# Setup logging for terminal output.
handler_stream = lg.StreamHandler()
handler_stream.setLevel(lg.DEBUG)
handler_stream.setFormatter(lg.Formatter("%(levelname)s:%(module)s: %(message)s"))

# Setup logging for file output.
handler_file = lg.FileHandler("thesis.log")
handler_file.setLevel(lg.INFO)
handler_file.setFormatter(
    lg.Formatter(
        fmt="%(asctime)s:%(levelname)s:%(module)s: %(message)s",
        datefmt="%Y-%m-%d.%H:%M:%S",
    )
)

# Setup logging for use across all sub-modules.
lg.basicConfig(
    level=lg.DEBUG,
    handlers=[handler_stream, handler_file],
)
